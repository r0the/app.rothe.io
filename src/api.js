const HOST = 'https://api.rothe.io'

function api (method, collection, element, data) {
  let uri = HOST + '/' + collection
  if (element) uri += '/' + element
  let options = {
    method: method,
    body: JSON.stringify(data),
    headers: new Headers()
  }
  options.headers.append('Authorization', 'Bearer ' + localStorage.getItem('rio.accessToken'))
  return new Promise(function (resolve, reject) {
    window.fetch(uri, options).then(response => {
      if (response.ok) {
        response.json().then(json => {
          resolve(json)
        })
      } else {
        response.text().then(text => {
          reject({
            response: text
          })
        })
      }
    }).catch(error => {
      reject(error)
    })
  })
}

const ACCESS_TOKEN = 'rio.accessToken';

export default {
  created: function () {
    this.accessToken = window.localStorage.getItem(ACCESS_TOKEN);
  },
  mounted: function () {
  },
  data: function () {
    return {
      accessToken: null,
      redirectUrl: null
    };
  },
  methods: {
    api: function (method, collection, element, data) {
      const vue = this;
      let uri = this.$options.host + '/' + collection;
      if (element) uri += '/' + element;
      const options = {
        method: method,
        body: JSON.stringify(data),
        headers: new window.Headers()
      };
      options.headers.append('Authorization', 'Bearer ' + this.accessToken);
      return new Promise(function (resolve, reject) {
        window.fetch(uri, options).then(response => {
          if (response.ok) {
            response.json().then(json => {
              response.json = json;
              resolve(response);
            });
          } else {
            if (response.status === 403) {
              vue.redirectUrl = vue.$route.fullPath;
              vue.$router.replace('/account/login');
            }
            resolve(response);
          }
        });
      });
    },
    accountLogin: function (name, password) {
      this.api('post', 'session', null, { name: name, password: password }).then(response => {
        if (response.ok) {
          this.updateToken(response.json.accessToken);
          this.$router.push(this.redirectUrl || '/');
        } else {
          console.log(response);
          if (response.status === 401) {
            this.$buefy.toast.open('Ungültiger Benutzername oder falsches Passwort.');
          } else {
            this.$buefy.toast.open('Login fehlgeschlagen.');
          }
        }
      });
    },
    accountLogout: function () {
      this.api('delete', 'session').then(response => {
        this.updateToken(null);
        this.$router.replace('/account/login');
      });
    },
    updateToken: function (accessToken) {
      this.accessToken = accessToken;
      window.localStorage.setItem(ACCESS_TOKEN, this.accessToken);
    }
  }
};

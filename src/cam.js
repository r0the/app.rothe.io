/* global AR, Vue */
(function () {
  const MD = navigator.mediaDevices

  Vue.component('v-cam', {
    props: {
      height: { type: Number, default: 320 },
      width: { type: Number, default: 240 }
    },
    data: function () {
      return {
        devices: [],
        playing: false,
        selectedDevice: 0
      }
    },
    computed: {
      playButton: function () {
        return this.playing ? 'Stop' : 'Play'
      }
    },
    created: function () {
      if (MD == null) return
      this.detector = new AR.Detector()
      MD.enumerateDevices().then(devs => {
        devs.forEach(dev => {
          if (dev.kind === 'videoinput') this.devices.push(dev)
        })
      })
    },
    methods: {
      tick: function () {
        const video = this.$refs.video
        const canvas = this.$refs.canvas
        canvas.width = parseInt(video.videoWidth)
        canvas.height = parseInt(video.videoHeight)
        canvas.style.width = canvas.width + 'px'
        canvas.style.height = canvas.height + 'px'
        const context = this.$refs.canvas.getContext('2d')
//        canvas.style.display = 'none'
        if (video.readyState === 4) {
          context.drawImage(video, 0, 0, canvas.width, canvas.height)
          const imageData = context.getImageData(0, 0, canvas.width, canvas.height)
          const markers = this.detector.detect(imageData)
          console.log(markers.length)
          this.drawCorners(markers)
          this.drawId(markers)
        }
        if (this.playing) requestAnimationFrame(this.tick)
      },
      drawCorners: function (markers) {
        const context = this.$refs.canvas.getContext('2d')
        var corners, corner, i, j;

        context.lineWidth = 3;
        for (i = 0; i !== markers.length; ++ i){
          corners = markers[i].corners;

          context.strokeStyle = "red";
          context.beginPath();

          for (j = 0; j !== corners.length; ++ j){
            corner = corners[j];
            context.moveTo(corner.x, corner.y);
            corner = corners[(j + 1) % corners.length];
            context.lineTo(corner.x, corner.y);
          }
          context.stroke();
          context.closePath();

          context.strokeStyle = "green";
          context.strokeRect(corners[0].x - 2, corners[0].y - 2, 4, 4);
        }
      },
      drawId: function (markers) {
        const context = this.$refs.canvas.getContext('2d')
        var corners, corner, x, y, i, j;

        context.strokeStyle = "blue";
        context.lineWidth = 1;

        for (i = 0; i !== markers.length; ++ i){
          corners = markers[i].corners;

          x = Infinity;
          y = Infinity;

          for (j = 0; j !== corners.length; ++ j){
            corner = corners[j];

            x = Math.min(x, corner.x);
            y = Math.min(y, corner.y);
          }
          context.strokeText(markers[i].id, x, y)
        }
      },
      toggle: function () {
        const video = this.$refs.video
        if (this.playing) {
          video.srcObject.getTracks().forEach(track => { track.stop() })
        } else {
          const id = this.devices[this.selectedDevice].deviceId
          MD.getUserMedia({ video: { deviceId: { exact: id } } }).then(stream => {
            video.srcObject = stream
            video.play()
            requestAnimationFrame(this.tick)
          })
        }
        this.playing = !this.playing
      }
    },
    template: `<div>
        <div><video id="video" :width="width" :height="height" ref="video"></video></div>
        <div class="field is-grouped">
          <div class="control"><div class="select"><select v-model="selectedDevice">
            <option v-for="(device, index) in devices" :value="index">{{ device.label }}</option>
          </select></div></div>
          <div class="control"><button class="button" @click="toggle">{{ playButton }}</button></div>
          <canvas ref="canvas" :width="width" :height="height"></canvas>
        </div>
      </div>`
  })
})()

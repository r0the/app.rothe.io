export default {
  data: function () {
    return {
      name: '',
      password: '',
      email: ''
    }
  },
  methods: {
    register: function () {
      this.$root.clearMessage();
      api('post', 'account', null, {
        name: this.name,
        password: this.password,
        email: this.email
      }).then(response => {
        this.$root.info('Registrierung erfolgreich, bitte anmelden.');
        this.$router.push('/account/login');
      }).catch(error => {
        this.$root.error(error.response);
        this.password = null;
      });
    },
    showLogin: function () {
      this.$root.clearMessage();
      this.$root.currentPage = 'v-page-login';
    }
  },
  template:
    `<div class="box">
      <h1>Registrieren</h1>
      <form>
        <b-field label="Benutzername">
          <b-input v-model="name" placeholder="Benutzername" icon="account" icon-pack="mdi"></b-input>
        </b-field>
        <b-field label="E-Mail-Adresse">
          <b-input v-model="email" type="email" placeholder="E-Mail" icon="email" icon-pack="mdi"></b-input>
        </b-field>
        <b-field label="Passwort">
          <b-input v-model="password" @keyup.enter.native="register" type="password" placeholder="Passwort" icon="key" icon-pack="mdi"></b-input>
        </b-field>
        <b-field grouped>
          <div class="control">
            <b-button @click="register" class="button is-primary">Registrieren</b-button>
          </div>
          <div class="control">
            <b-button @click="$router.go(-1)" class="button is-secondary">Zurück</b-button>
          </div>
        </b-field>
      </form>
    </div>`
};

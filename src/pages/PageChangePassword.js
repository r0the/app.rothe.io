export default {
  data: function () {
    return {
      password: '',
      newPassword: ''
    };
  },
  methods: {
    changePassword: function () {
      this.api('put', 'account', 'self', {
        password: this.password,
        newPassword: this.newPassword
      });
    }
  },
  template:
    `<div class="box">
      <h1>Konto</h1>
      <b-field label="bisheriges Passwort">
        <b-input v-model="password" type="password" placeholder="bisheriges Passwort" icon="key" icon-pack="mdi"></b-input>
      </b-field>
      <b-field label="neues Passwort">
        <b-input v-model="newPassword" @keyup.enter.native="changePassword" type="password" placeholder="neues Passwort" icon="key" icon-pack="mdi"></b-input>
      </b-field>
      <b-field grouped>
        <div class="control">
          <b-button @click="changePassword" class="button is-primary">Passwort ändern</b-button>
        </div>
      </b-field>
    </div>`
};

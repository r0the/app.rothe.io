export default {
  created: function () {
    this.$root.api('delete', 'session', null).then(response => {
      if (!response.ok) this.$router.back();
    })
  },
  template: `<div>
      <h1>Abmeldung</h1>
      <p>Du bist abgemeldet worden.</p>
    </div>`
};

export default {
  props: {
    register: { type: Boolean, default: false }
  },
  created: function () {
    console.log('Login route:', this.$route);
  },
  data: function () {
    return {
      name: '',
      password: ''
    };
  },
  methods: {
    login: function () { this.$root.accountLogin(this.name, this.password); }
  },
  template:
    `<div class="box">
      <h1>Login</h1>
      <form>
        <b-field label="Benutzername">
          <b-input v-model="name" placeholder="Benutzername" icon="account" icon-pack="mdi"></b-input>
        </b-field>
        <b-field label="Passwort">
          <b-input v-model="password" @keydown.enter.native="login" type="password" placeholder="Passwort" icon="key" icon-pack="mdi"></b-input>
        </b-field>
        <b-field grouped>
          <div class="control">
            <b-button @click="login" class="button is-primary">Anmelden</b-button>
          </div>
          <div v-if="register" class="control">
            <router-link tag="button" class="button is-secondary" to="/account/register">Kein Konto?</router-link>
          </div>
        </b-field>
      </form>
    </div>`
};

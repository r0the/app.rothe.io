export default {
  data: function () {
    return {
      account: null
    };
  },
  created: function () {
    this.$root.api('get', 'account', 'self').then(response => {
      this.account = response.json;
    });
  },
  template: `
    <div v-if="account">
      <p>Name: {{ account.name }}</p>
      <p>E-Mail: {{ account.email }}</p>
      <p>Rollen: {{ account.roles }}</p>
      <b-button type="is-primary" tag="router-link" to="/account/logout">Abmelden</b-button>
    </div>
  `
};

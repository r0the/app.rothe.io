export default {
  data: function () {
    return {
      accounts: [],
      columns: [
        { field: 'id', label: 'ID', numeric: true },
        { field: 'name', label: 'Konto' },
        { field: 'email', label: 'E-Mail' },
        { field: 'roles', label: 'Rollen' }
      ],
      selected: null
    };
  },
  created: function () {
    this.api('get', 'account').then(response => {
      this.accounts = response;
    }).catch(error => {
      this.$root.error(error.response);
    });
  },
  template:
    `<div>
      <h1>Kontoverwaltung</h1>
      <b-table :data="accounts" :columns="columns" :selected.sync="selected"></b-table>
    </div>`
};

/* global Vue, VueRouter */
import API from './common/API.js';
import PageAdminAccounts from './pages/PageAdminAccounts.js';
import PageChangePassword from './pages/PageChangePassword.js';
import PageContent from './pages/PageContent.js';
import PageLogin from './pages/PageLogin.js';
import PageLogout from './pages/PageLogout.js';
import PageProfile from './pages/PageProfile.js';
import PageRegister from './pages/PageRegister.js';

const PageMain = {
  template:
    `<div>
      <h1>Hauptseite</h1>
    </div>`
};

const PageNotFound = {
  computed: {
    rt: function () {
      return this.$route.path;
    }
  },
  template: `
    <div>
    NOT FOUND: {{ rt }}
    </div>
  `
};

const routes = [
  { path: '/', component: PageMain },
  { path: '/admin/accounts', component: PageAdminAccounts },
  { path: '/account/login', component: PageLogin },
  { path: '/account/logout', component: PageLogout },
  { path: '/account/profile', component: PageProfile },
  { path: '/account/register', component: PageRegister },
  { path: '/account/password', component: PageChangePassword },
  { path: '/content', component: PageContent },
  { path: '*', component: PageNotFound }
];

const router = new VueRouter({ routes: routes });

const app = new Vue({
  router,
  host: 'https://api.rothe.io',
  mixins: [API],
  el: '#app',
  appName: 'TEST',
  template: `
    <div>
      <b-navbar fixed-top>
        <template slot="end">
          <b-navbar-item tag="router-link" :to="{ path: '/account/profile' }">
            <b-icon icon="account-circle"></b-icon>
          </b-navbar-item>
        </template>
      </b-navbar>
      <div class="container content">
        <router-view></router-view>
      </div>
    </div>
  `
});

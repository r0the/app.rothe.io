Vue.component('v-challenge', {
  props: {
    id: { type: Number },
    title: { type: String }
  },
  data: function () {
    return {
      info: {},
      answer: ''
    }
  },
  created: function () {
    api('get', 'solution', this.id).then(response => {
      this.info = response
    }).catch(error => {
      this.$root.error(error.response)
    })
  },
  methods: {
    check: function () {
      let anwser = this.answer
      let data = { id: this.id, answer: anwser }
      api('post', 'solution', null, data).then(result => {
        console.log('Result: '+result)
      })
    }
  },
  template: `
    <div class="card">
      <header class="card-header"><p class="card-header-title">{{ title }}</p></header>
      <div class="card-content"><slot></slot></div>
      <footer class="card-footer">
        <div v-if="info.solved" class="card-footer-item">
          Du hast diese Aufgabe schon gelöst ({{ info.solved }}).
        </div>
        <div v-if="!info.solved" class="field has-addons card-footer-item">
          <div class="control"><input v-model="answer" class="input" placeholder="Lösung"></div>
          <div class="control"><button class="button is-primary" @click="check">Überprüfen</button></div>
        </div>
      </footer>
    </div>`
})

PageChallenges = {
  data: function () {
    return {
      items: []
    }
  },
  created: function () {
    api('get', 'challenge').then(response => {
      this.items = response
    }).catch(error => {
      this.$root.error(error.response)
    })
  },
  template:
    `<div><h1>Aufgaben</h1>
      <v-challenge :id="1" title="Aufgabe 1">
        Was gibt 10011001 plus 01100110 als Dezimalzahl?
      </v-challenge>
      <v-challenge :id="3" title="Aufgabe 2">
        Erste Programmierin und Programmiersprache
      </v-challenge>
    </div>`
}
